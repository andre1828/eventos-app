var doc = document;
var listaEventosFavoritos = doc.getElementById('listaEventosFavoritos');

var eventosFavoritados = carregarEventosLocalStorage(); //  cria lista eventosFavoritados

var eventoTemplateFavorito = doc.getElementById('eventoTemplateFavorito');

var fragmento = doc.createDocumentFragment();
for (var i = 0; i < eventosFavoritados.length; i++) {
  var templateClone = eventoTemplateFavorito.content.cloneNode(true);
  templateClone.querySelector('#eventoImg').src = eventosFavoritados[i].img
  templateClone.querySelector('#eventoNome').innerHTML = eventosFavoritados[i].nome;
  templateClone.querySelector('#eventoLocal').innerHTML = eventosFavoritados[i].local;
  templateClone.querySelector('#eventoData').innerHTML = eventosFavoritados[i].data;
  templateClone.querySelector('#btnFav').setAttribute("onclick", `removerEventoFavorito(${i})`);

  fragmento.appendChild(templateClone);
}
listaEventosFavoritos.appendChild(fragmento);

function removerEventoFavorito(index){
  // TODO implementar
}




function carregarEventosLocalStorage() {
      var eventosFavoritados;
      if(localStorage.listaEventosFavoritos) {
        eventosFavoritados = JSON.parse(localStorage.listaEventosFavoritos);
      }
      else {
        eventosFavoritados = [];
      }

      return eventosFavoritados;
}
