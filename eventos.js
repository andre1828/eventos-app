var doc = document;

var listaEventos = doc.getElementById('listaEventos');

var eventoTemplate = doc.getElementById('eventoTemplate');

var eventosFavoritados = [];

var eventos = [
  evento1 = {
    id : 0,
    img : "imgs/img1.jpg",
    nome: "Fortal",
    local : "Mei da rua",
    data : "30/12/2016"
  },
  evento2 = {
    id : 1,
    img : "imgs/img2.jpg",
    nome: "Showzao pauleira",
    local : "Casa do Mafra",
    data : "12/11/2016"
  },
  evento3 = {
    id : 2,
    img : "imgs/img3.jpg",
    nome: "Churrasquim de cachorro",
    local : "China",
    data : "13/11/2016"
  }
]

var fragmento = doc.createDocumentFragment();

for (var i = 0; i < eventos.length; i++) {
  var templateClone = eventoTemplate.content.cloneNode(true);
  templateClone.querySelector('#eventoImg').src = eventos[i].img
  templateClone.querySelector('#eventoNome').innerHTML = eventos[i].nome;
  templateClone.querySelector('#eventoLocal').innerHTML = eventos[i].local;
  templateClone.querySelector('#eventoData').innerHTML = eventos[i].data;
  templateClone.querySelector('#btnFav').setAttribute("onclick", `addEventoFavorito(${i})`);

  fragmento.appendChild(templateClone);
}

listaEventos.appendChild(fragmento);

function addEventoFavorito(index) {
  //TODO se clicar no evento de novo, adicionará evento repetido
  if(localStorage.listaEventosFavoritos) {
    listaEventosFavoritos = JSON.parse(localStorage.listaEventosFavoritos);
    listaEventosFavoritos.push( eventos[index] );
    var listaEventosFavoritosJSON = JSON.stringify(listaEventosFavoritos);
    localStorage.setItem("listaEventosFavoritos", listaEventosFavoritosJSON);
  } else {
    eventosFavoritados.push(eventos[index]);
    var eventosFavoritadosJSON = JSON.stringify(eventosFavoritados);
    localStorage.setItem("listaEventosFavoritos", eventosFavoritadosJSON);
  }
}
